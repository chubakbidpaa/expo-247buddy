import React from "react";
import ChatPage from "../src/components/stateful/Chat/ChatPage";
import renderer from "react-test-renderer";

describe("ChatPage", () => {
  describe("Rendering", () => {
    it("should match the snapshot", () => {
      const tree = renderer.create(<ChatPage />).toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});

describe("ChatPage Render", () => {
  it("has 1 child", () => {
    const tree = renderer.create(<ChatPage />).toJSON();
    expect(tree.children.length).toBe(2);
  });
});
