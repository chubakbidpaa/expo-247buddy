/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import * as React from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import ChatView from "../modules/chat/ChatView";

export default function ChatScreen() {
  const navigation = useNavigation();
  const route = useRoute();


  return <ChatView switcher="start" navigation={navigation} />;
}
