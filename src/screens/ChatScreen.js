/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import * as React from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import ChatView from "../modules/chat/ChatView";

export default function ChatScreen() {
  const navigation = useNavigation();
  return <ChatView switcher="chat" navigation={navigation}/>;
}
