/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import * as React from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import ListenerView from "../modules/listener/ListenerViewContainer";

export default function VolunteerZone() {
  const navigation = useNavigation();

  const route = useRoute();

  if (route.params.token) {
    navigation.navigate("UserPanelScreen");
    return true;
  }

  return <ListenerView navigation={navigation} switcher="zone" />;
}
