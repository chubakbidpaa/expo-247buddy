/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import React, { Component } from "react";
import { StyleSheet, Image, ImageBackground } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Icon, Text, Button, Overlay } from "react-native-elements";
import { TouchableHighlight } from "react-native-gesture-handler";
import * as en from "../../localization/en.json";
const backgroundImage = require("../../../assets/img/gradient-2.png");

export default class VolunteerZone extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <ImageBackground style={styles.bgImage} source={backgroundImage}>
        <Grid>
            <Row size={130} />
          <Row size={20}>
            <Button
              icon={{
                type: "font-awesome",
                name: "sign-in",
                color: "white",
              }}
              title={en.landingPage.logIn}
              onPress={() => navigation.navigate("LoginScreen")}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
            />
          </Row>
          <Row size={150}>
            <Button
              icon={{
                type: "font-awesome",
                name: "user-plus",
                color: "white",
              }}
              title={en.landingPage.register}
              onPress={() => navigation.navigate("RegisterScreen")}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
            />
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "space-around",
    padding: 20,
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  heroImage: {
    resizeMode: "contain",
    height: "80%",
    width: "86%",
    alignSelf: "center",
  },
  buttonContainer: {
    width: "85%",
    marginLeft: 20
  },
  buttonContainerModal: {
    width: "100%",
    borderBottomColor: "#000",
  },
  button: {
    backgroundColor: "#124",
    borderRadius: 20,
  },
  buttonModal: {
    backgroundColor: "#124",
    borderRadius: 20,
    alignSelf: "center",
    borderBottomWidth: 5,
  },
  overlay: {
    width: "70%",
    height: "20%",
    backgroundColor: "#124",
    alignSelf: "center",
    borderRadius: 20,
  },
});
