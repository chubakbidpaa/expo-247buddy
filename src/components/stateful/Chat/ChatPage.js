import React, { Component } from "react";
import { Icon } from "react-native-elements";
import { ActivityIndicator, ToastAndroid } from "react-native";
import { Grid, Row, Col } from "react-native-easy-grid";
import ChatInput from "./ChatInput";
import ChatMessage from "./ChatMessage";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import io from "socket.io-client";

const URL = "https://demo-247buddy.herokuapp.com";

export default class ChatPage extends Component {
  constructor(props) {
    super(props);

    this.addMessage = this.addMessage.bind(this);
    this.submitMessage = this.submitMessage.bind(this);
  }


  componentDidMount() {
    const {messageWatch, partnerLeft, navigation} = this.props;

    messageWatch();

    if (partnerLeft) {
      ToastAndroid.showWithGravity("Partner left the chat", ToastAndroid.SHORT, ToastAndroid.CENTER);
      navigation.navigate("WelcomeScreen");
    }

  }


  async submitMessage(messageText) {
    const {sendMessage} = this.props;

    await sendMessage(messageText);

  }

  render() {
    console.log(this.state.user);
    return (
      <Grid>
        <Row>
          {this.props.messages.map((message) => {
            return (
              <Row>
                <ChatMessage
                  style={this.props.user}
                  key={Math.random()}
                  message={message.msg}
                  name={message.user}
                />
              </Row>
            );
          })}
        </Row>
        <Row>
          <ChatInput
            addMessage={this.submitMessage}
            socket={this.socket}
            name={this.name}
            session={this.state.sessionId}
          />
        </Row>
      </Grid>
    );
  }
}
