import React, { Component } from "react";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  Text,
  Button,
} from "react-native-elements";

export default class ChatInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: null,
      icon: "comment",
    };
  }

  render() {
    return (
      <Grid>
        <Col size={90}>
          <Input
            multiline
            numberOfLines={4}
            leftIcon={{ name: this.state.icon, type: "font-awesome" }}
            onChangeText={(t) => {
              this.setState({ message: t, icon: "comment-o" });
              this.props.socket.to(this.props.sessionId).emit("typing", {
                from: this.props.name,
              });
            }}
            onBlur={() => {
              this.setState({ icon: "comment" });
              this.props.socket
                .to(this.props.sessionId)
                .emit("stoptyping", { from: this.props.name });
            }}
          />
        </Col>
        <Col size={10}>
          <Button
            icon={{ type: "font-awesome", name: "paper-plane" }}
            onPress={() => this.props.addMessage(this.state.message)}
          />
        </Col>
      </Grid>
    );
  }
}
