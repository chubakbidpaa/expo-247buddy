import React from "react";
import { Grid, Col, Row } from "react-native-easy-grid";
import { Text } from "react-native-elements";

export default function ChatMessage({ name, message }) {
  return (
    <Grid>
      <Row>
        <Text>{name}</Text>
      </Row>
      <Row>
        <Text>{message}</Text>
      </Row>
    </Grid>
  );
}
