import React, { Component } from "react";
import {
  View,
  Vibration,
  Platform,
  StyleSheet,
  ImageBackground
} from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  Text,
  Button
} from "react-native-elements";

const image = require("../../../assets/img/gradient-2.png");

export default class SeekerLoungeComponent extends Component {
  state = {
    timer: 180
  };

  componentDidMount() {

    const { seekerLounge, navigation } = this.props;

    setTimeout(() => {
      this.setState({ time: this.state.timer - 1 });
    }, 1);

    seekerLounge().then(res => {
      if (res) {
        navigation.navigate("ChatScreen");
      } else {
        navigation.navigate("WelcomeScreen");
      }
    });

  }

  onCancelSession() {
    const { navigation, cancelSession } = this.props;

    cancelSession().then((res) => {
      if (res) {
        navigation.navigate("WelcomeScreen");
      }
    });

  }

  render() {
    return (
      <Grid>
        <Row size={50}>
          <Text h1>{this.state.timer}</Text>
        </Row>
        <Row>
          <Button title="Cancel" onPress={() => this.onCancelSession()}/>
        </Row>
      </Grid>
    );
  }

}