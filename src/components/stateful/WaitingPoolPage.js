import React, { Component } from "react";
import { StyleSheet, ImageBackground, View } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  CheckBox,
  Text,
  Button,
  ButtonGroup,
  List
} from "react-native-elements";

const backgroundImage = require("../../../assets/img/gradient-2.png");


export default class WaitingPoolComponent extends Component {
  componentDidMount() {
    const { getPool, updatePool, updateAuto } = this.props;

    getPool();

    if (updateAuto) {
      updatePool();
    }

  }

  toggleUpdateAuto() {
    const { updatePool, toggleUpdateAuto, updateAuto } = this.props;

    toggleUpdateAuto(!updateAuto);

    if (!updateAuto) {
      updatePool();
    }
  }

  renderList() {

    const { acceptSession, navigation } = this.props;

    this.props.pool.map(l => {
      return (<Grid>
          <Row>
            <Text h2>{l.seekerReason}</Text>
          </Row>
          <Row>
            <Col size={5}>
              <Text h4>Requested at: </Text>
            </Col>
            <Col>
              <Text>{l.requestedAt}</Text>
            </Col>
          </Row>
          <Row>
            <Button onPress={() => {
              acceptSession(l.sessionId).then(() => navigation.navigate("ChatScreen"));
            }} title="Accept"/>
          </Row>
        </Grid>
      );
    });
  }

  render() {
    const { updateAuto } = this.props;
    return (
      <View>
        <View>
          <CheckBox checked={updateAuto} onPress={() => this.toggleUpdateAuto()} title="Update Automatically"/>
        </View>
        <List>
          {this.renderList()}
        </List>
      </View>
    );
  }
}