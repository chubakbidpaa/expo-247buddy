/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import * as React from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  Text,
  Button
} from "react-native-elements";
import * as en from "../../localization/en.json";
import * as helpers from "../../helpers";

export default class LoginPageComponent extends React.Component {
  state = {
    number: null,
    otp: null,
    otpSent: false,
    disabled: false,
    inputDisabled: true
  };

  requestOtp = async () => {

    await this.setState({ disabled: true });

    if (!this.state.otpSent) {
      await this.props.requestOtp(this.state.number, "true");
      await this.setState({ disabled: false, inputDisabled: false });
    }
  };

  authAndRedirect = async () => {
    const { navigation, authListener } = this.props;

    await this.setState({ disabled: true });


    const authRes = await authListener(this.state.number, this.state.otp);

    if (authRes) {
      navigation.navigate("UserPanelScreen");
    } else {
      await this.setState({ disabled: false });
    }
  };

  render() {
    const image = require("../../../assets/img/gradient-2.png");
    return (
      <ImageBackground source={image} style={styles.bgImage}>
        <Grid style={styles.mainContainer}>
          <Row size={30}>
            <Col size={100}>
              <Input
                leftIcon={{
                  type: "font-awesome",
                  color: "#fff",
                  name: "user"
                }}
                label={en.listenerLoginPage.loginString}
                labelStyle={styles.input}
                defaultValue="+91"
                onChangeText={(t) => this.setState({ number: t })}
              />
            </Col>
          </Row>
          <Row size={30}>
            <Col size={90}>
              <Input
                leftIcon={{
                  type: "font-awesome",
                  color: this.state.disabled || !this.props.otpSent ? "red" : "white",
                  name: this.state.disabled || !this.props.otpSent ? "lock" : "key"
                }}
                label={en.listenerLoginPage.password}
                labelStyle={styles.input}
                disabled={this.state.disabled || !this.props.otpSent}
                onChangeText={(t) => this.setState({ otp: t })}
              />
            </Col>
          </Row>
          <Row size={120}>
            <Button
              icon={{
                type: "font-awesome",
                name: "sign-in",
                color: "white"
              }}
              title={this.props.otpSent ? "Log in" : "Request OTP"}
              onPress={() => {
                this.props.otpSent ? this.authAndRedirect() : this.requestOtp();
              }}
              buttonStyle={styles.buttonStyle}
              containerStyle={styles.buttonContainerStyle}
              disabled={this.state.disabled}
            />
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  form: {
    marginTop: 100
  },
  itemContainer: {
    margin: 20
  },
  icon: {
    color: "#fff"
  },
  input: {
    color: "#fff"
  },
  submitButton: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center"
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  buttonContainerStyle: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center"
  }
});
