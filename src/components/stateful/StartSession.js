import React, { Component } from "react";
import {
  Text,
  View,
  Vibration,
  Platform,
  StyleSheet,
  ImageBackground
} from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  Button
} from "react-native-elements";

const image = require("../../../assets/img/gradient-2.png");



export default class StartSessionComponent extends Component {
  state = {
    number: null,
    reason: null
  };

  async onStartSession() {
      const {startSession, navigation} = this.props;

      await startSession(this.state.number, this.state.reason);
      navigation.navigate("SeekerLoungeScreen")
  }

  render() {
    return (
      <ImageBackground style={styles.bgImage} source={image}>
        <Grid>
          <Row>
            <Input
              label="Your Number"
              labelStyle={styles.input}
              onChangeText={(t) => this.setState({ number: t })}
            />
          </Row>
          <Row>
            <Input
              label="Your Reason"
              labelStyle={styles.input}
              onChangeText={(t) => this.setState({ reason: t })}
            />
          </Row>
          <Row>
            <Button title="Ask for Help" onPress={() => this.onStartSession()}/>
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  form: {
    marginTop: 100
  },
  itemContainer: {
    margin: 20
  },
  icon: {
    color: "#fff"
  },
  input: {
    color: "#fff"
  },
  submitButton: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center"
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  buttonContainerStyle: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center"
  }
});
