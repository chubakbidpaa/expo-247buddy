import React, { Component } from "react";
import { ImageBackground, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Button, Tooltip, Text, Icon } from "react-native-elements";

const image = require("../../../assets/img/gradient-2.png");

export default class UserPanelComponent extends Component {
  state = {
    disabled: false,
    secondDisabled: false,
  };

  onLogout = async () => {
    await this.setState({ disabled: true });

    const logoutRes = await this.props.logOut();

    if (logoutRes) {
      this.props.navigation.navigate("WelcomeScreen");
    } else {
      await this.setState({ disabled: false });
    }
  };

  onToggleAvailable = async () => {
    await this.setState({ secondDisabled: true });

    await this.props.toggleAvailable();

    await this.setState({ secondDisabled: false });
  };

  render() {
    return (
      <ImageBackground
        style={styles.bgImage}
        source={image}
      >
        <Grid style={styles.bgImage}>
          <Row>
            <Button
              title="Logout"
              onPress={async () => {
                await this.onLogout();
              }}
              icon={{ type: "font-awesome", name: "sign-out", color: "#fff" }}
              containerStyle={styles.buttonContainerStyle}
              buttonStyle={styles.submitButton}
              disabled={this.state.disabled}
            />
          </Row>
          <Row>
            <Col size={10}>
              <Button
                title={this.props.isAvailable ? "Go Offline" : "Go Online"}
                onPress={() => this.onToggleAvailable()}
                icon={{
                  type: "font-awesome",
                  name: this.props.isAvailable ? "toggle-on" : "toggle-off",
                  color: "#fff",
                }}
                containerStyle={styles.buttonContainerStyle}
                buttonStyle={styles.submitButton}
                disabled={this.state.secondDisabled}
              />
            </Col>

            <Col size={4}>
              <Tooltip
                popover={
                  <Text>
                    Going offline means, you'll not get notification from the
                    listeners.
                  </Text>
                }
              >
                <Icon type="font-awesome" color="#fff" name="question-circle" />
              </Tooltip>
            </Col>
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  form: {
    marginTop: 100,
  },
  itemContainer: {
    margin: 20,
  },
  icon: {
    color: "#fff",
  },
  input: {
    color: "#fff",
  },
  submitButton: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center",
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  buttonContainerStyle: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center",
  },
});
