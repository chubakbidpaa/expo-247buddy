/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import React, { Component } from "react";
import { StyleSheet, Image, ImageBackground } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Icon, Text, Button, Overlay } from "react-native-elements";
import { TouchableHighlight } from "react-native-gesture-handler";
import * as en from "../../localization/en.json";
import { compose } from "recompose";
import { connect } from "react-redux";
import * as ListenerState from "../../modules/listener/ListenerState";

class LoginPageComponent extends Component {
  state = { modalVisible: false };

  componentDidMount() {
    if (this.props.isChatting) {
      this.props.navigation.navigate("ChatScreen");
    }
  }


  toggleOverlay = () => {
    this.setState({ modalVisible: !this.state.modalVisible });
  };

  render() {
    const backgroundImage = require("../../../assets/img/gradient-2.png");
    const heroImage = require("../../../assets/img/hero.png");
    const { navigation } = this.props;
    return (
      <ImageBackground source={backgroundImage} style={styles.bgImage}>
        <Grid style={styles.mainContainer}>
          <Row size={40}>
            <Image style={styles.heroImage} source={heroImage}/>
          </Row>

          <Row size={10}>
            <Button
              icon={{
                type: "font-awesome",
                name: "user-circle",
                color: "white"
              }}
              title={en.landingPage.volunteer}
              onPress={() =>
                navigation.navigate("VolunteerZoneScreen", {
                  token: this.props.token
                })
              }
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
            />
          </Row>
          <Row size={10}>
            <Button
              icon={{
                type: "font-awesome",
                name: "comment",
                color: "white"
              }}
              title={en.landingPage.talk}
              onPress={() => navigation.navigate("StartSessionScreen")}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
            />
          </Row>
          <Row size={50}>
            <Button
              icon={{
                type: "font-awesome",
                name: "gift",
                color: "white"
              }}
              title={en.landingPage.donate}
              onPress={() => navigation.navigate("DonateScreen")}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
            />
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "space-around",
    padding: 20
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  heroImage: {
    resizeMode: "contain",
    height: "80%",
    width: "86%",
    alignSelf: "center"
  },
  buttonContainer: {
    width: "85%"
  },
  buttonContainerModal: {
    width: "100%",
    borderBottomColor: "#000"
  },
  button: {
    backgroundColor: "#124",
    borderRadius: 20
  },
  buttonModal: {
    backgroundColor: "#124",
    borderRadius: 20,
    alignSelf: "center",
    borderBottomWidth: 5
  },
  overlay: {
    width: "70%",
    height: "20%",
    backgroundColor: "#124",
    alignSelf: "center",
    borderRadius: 20
  }
});

export default compose(
  connect(
    (state) => ({
      cellNumber: state.listener.cellNumber,
      otpSent: state.listener.otpSent,
      token: state.listener.token,
      isAvailable: state.listener.isAvailable,
      isChatting: state.chat.isChatting
    }),
    (dispatch) => ({
      registerListener: (userName, number, bio, isTest) =>
        dispatch(ListenerState.registerListener(userName, number, bio, isTest)),
      requestOtp: (number, isTest) =>
        dispatch(ListenerState.requestOtp(number, isTest)),
      authListener: (number, otp) =>
        dispatch(ListenerState.authListener(number, otp)),
      logOut: () => dispatch(ListenerState.logoutListener()),
      toggleAvailable: () => dispatch(ListenerState.toggleActiveStatus())
    })
  )
)(LoginPageComponent);
