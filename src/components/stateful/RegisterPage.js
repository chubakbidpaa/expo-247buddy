/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */
import React, { Component } from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Tooltip,
  Icon,
  Input,
  Image,
  Text,
  Button,
  ButtonGroup,
} from "react-native-elements";
import * as helpers from "../../helpers/";
import * as en from "../../localization/en.json";
const backgroundImage = require("../../../assets/img/gradient-2.png");
const buttons = ["Depression", "Suicidal Thoughts", "Romantic Issues"];

export default class LoginPageComponent extends Component {
  state = {
    name: "",
    bio: "",
    number: "",
    numberErr: "",
    valid: false,
    countryCode: "+91",
    disabled: false,
  };

  updateIndex(newIndices) {
    this.setState({
      selectedIndices: [...newIndices],
    });
  }

  onRegisterUser = async () => {
    const { navigation, registerListener } = this.props;

    await this.setState({ disabled: true });

    const registerRes = await registerListener(
      this.state.name.toString(),
      this.state.number.toString(),
      this.state.bio.toString(),
      "true"
    );

    if (registerRes) {
      navigation.navigate("LoginScreen");
    } else {
      await this.setState({ disabled: false });
    }
  };

  onValidatePhoneNumber = (v) => {
    const pattern = / ^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$ /;
    if (pattern.test(v)) {
      this.setState({ valid: true, number: v });
    } else {
      this.setState({
        valid: false,
        numberErr: en.registerPage.numberErr,
      });
    }
  };

  render() {
    const { selectedIndices } = this.state;
    return (
      <ImageBackground source={backgroundImage} style={styles.bgImage}>
        <Grid>
          <Row size={30} style={styles.inputField}>
            <Col size={90}>
              <Input
                leftIcon={{ type: "font-awesome", name: "user", color: "#fff" }}
                label={en.registerPage.name}
                labelStyle={styles.inputLabelStyle}
                inputStyle={styles.inputStyle}
                onChangeText={(v) => this.setState({ name: v })}
              />
            </Col>
          </Row>
          <Row size={30} style={styles.inputField}>
            <Col size={90}>
              <Input
                leftIcon={{
                  type: "font-awesome",
                  name: "info",
                  color: "#fff",
                }}
                label={en.registerPage.bio}
                labelStyle={styles.inputLabelStyle}
                inputStyle={styles.inputStyle}
                multiline
                numberOfLines={6}
                onChangeText={(v) => this.setState({ bio: v })}
              />
            </Col>
          </Row>
          <Row size={50} style={styles.inputField}>
            <Input
              leftIcon={{
                type: "font-awesome",
                name: "mobile",
                color: "#fff",
              }}
              defaultValue={this.state.countryCode}
              label={en.registerPage.phoneNumber}
              labelStyle={styles.inputLabelStyle}
              inputStyle={styles.inputStyle}
              maxLength={13}
              onChangeText={(v) => {
                this.setState({ number: v });
              }}
            />
          </Row>
          <Row size={20}>
            <Col size={90}>
              <Button
                icon={{
                  type: "font-awesome",
                  name: "user-plus",
                  color: "white",
                }}
                buttonStyle={styles.submitButtonStyle}
                title={en.registerPage.register}
                onPress={() => this.onRegisterUser()}
                disabled={this.state.disabled}
              />
            </Col>
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    padding: 20,
  },
  inputField: {
    marginTop: 20,
  },
  input: {
    color: "#fff",
    borderBottomWidth: 20,
  },
  submitButtonStyle: {
    backgroundColor: "#124",
    borderRadius: 20,
    width: "80%",
    alignSelf: "center",
  },
  label: {
    color: "#582630",
    textShadowColor: "#F1A66A",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 13,
  },
  bgImage: { flex: 1, width: "100%", height: "100%" },
  logo: {
    resizeMode: "contain",
    width: "80%",
    height: "80%",
    marginLeft: 20,
    marginTop: -10,
  },
  phoneNumberInput: {
    flex: 1,
    flexDirection: "row",
  },
  phoneNumberHighlight: {
    flex: -1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: -27,
    marginLeft: "90%",
  },
  bullseye: {
    marginLeft: -25,
    fontSize: 40,
  },
  buttonGroupContainerStyle: { height: 50 },
  buttonGroupStyle: {
    backgroundColor: "#bd2a2a",
  },
  buttonGroupTextStyle: {
    color: "#ff6",
  },
  buttonGroupSelectedStyle: {
    backgroundColor: "#ff0241",
  },
  buttonGroupSelectedTextStyle: {
    color: "#fff",
  },
  inputLabelStyle: {
    color: "#f5bcdb",
  },
});
