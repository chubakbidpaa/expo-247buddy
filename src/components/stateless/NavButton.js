import * as React from "react";
import { Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

export default function ({ screen }) {
  const navigation = useNavigation();
  return <Button title={screen} onPress={() => navigation.navigate(screen)} />;
}
