import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

//ComponentImport

import LandingPageScreen from "../screens/LandingScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import VolunteerZoneScreen from "../screens/VolunteerZoneScreen";
import ChatScreen from "../screens/ChatScreen";
import StartSessionScreen from "../screens/StartSessionScreen";
import UserPanelScreen from "../screens/UserPanelScreen";
import SeekerLoungeScreen from "../screens/SeekerLoungeScreen";

const Stack = createStackNavigator();

export default function NavigationStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="WelcomeScreen">
        {/* ScreenNames */}
        <Stack.Screen
          name="WelcomeScreen"
          component={LandingPageScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ title: "Login as Listener" }}
        />
        <Stack.Screen
          name="RegisterScreen"
          component={RegisterScreen}
          options={{ title: "Register Listener" }}
        />

        <Stack.Screen
          name="VolunteerZoneScreen"
          component={VolunteerZoneScreen}
          options={{ title: "Volunteer Zone" }}
        />
        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="StartSessionScreen"
          component={StartSessionScreen}
          options={{ title: "Start Session" }}
        />
        <Stack.Screen
          name="UserPanelScreen"
          component={UserPanelScreen}
          options={{ title: "Volunteer Panel" }}
        />
        <Stack.Screen name="SeekerLoungeScreen" component={SeekerLoungeScreen} options={{ title: "Seeker Lounge" }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
