// ES6 module syntax
import LocalizedStrings from "react-native-localization";



let strings = new LocalizedStrings({
  en: {
    "login": "Login",
    "register": "Register",
    "talk": "Talk to Someone",
    "volunteer": "Volunteer Area",
    "logout": "Logout",
    "donate": "Donate",
    "send": "Send",
    
  },
  it: {
    how: "Come vuoi il tuo uovo oggi?",
    boiledEgg: "Uovo sodo",
    softBoiledEgg: "Uovo alla coque",
    choice: "Come scegliere l'uovo",
  },
});
