import { combineReducers } from "redux";
//NOTE: do NOT remove these comments.

//imports

import listener from "../modules/listener/ListenerState";
import auth from "../modules/auth/AuthState";
import chat from "../modules/chat/ChatState";
import pool from "../modules/pool/PoolState"

export default combineReducers({
  //combines
  listener,
  auth,
  chat,
  pool
});
