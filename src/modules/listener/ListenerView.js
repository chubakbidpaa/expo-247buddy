/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

import React, { Component } from "react";
import LoginPage from "../../components/stateful/LoginPage";
import RegisterPage from "../../components/stateful/RegisterPage";
import VolunteerZone from "../../components/stateful/VolunteerZonePage";
import UserPanelPage from "../../components/stateful/UserPanelPage";
import { TestHookStore } from "cavy";

export default class FirebaseAuthView extends Component {
  render() {
    if (this.props.switcher === "login") {
      return (
        <LoginPage
          authListener={this.props.authListener}
          navigation={this.props.navigation}
          otpSent={this.props.otpSent}
          requestOtp={this.props.requestOtp}
        />
      );
    } else if (this.props.switcher === "register") {
      return (
        <RegisterPage
          registerListener={this.props.registerListener}
          navigation={this.props.navigation}
        />
      );
    } else if (this.props.switcher === "zone") {
      return <VolunteerZone navigation={this.props.navigation} />;
    } else if (this.props.switcher === "panel") {
      return (
        <UserPanelPage
          logOut={this.props.logOut}
          toggleAvailable={this.props.toggleAvailable}
          isAvailable={this.props.isAvailable}
          navigation={this.props.navigation}
        />
      );
    }
    return false;
  }
}
