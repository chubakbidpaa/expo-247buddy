/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

import axios from "axios";
import * as CONSTANTS from "./ListenerConstants";
import * as helpers from "../../helpers";
import * as globalStr from "../../../global";
import * as ENV from "../../../env";
import { RSAKeychain } from "react-native-rsa-native";
import { ToastAndroid, Alert } from "react-native";

//initialState
const initialState = {
  userName: null,
  token: null,
  approved: null,
  dateRegistered: null,
  email: null,
  emailVerified: false,
  cellNumber: null,
  avatar: null,
  categories: null,
  banned: null,
  status: {
    engagedSession: null,
    online: null
  },
  error: null,
  message: null,
  otpSent: false,
  isAvailable: false
};

//thunk functions

export function getListener(listenerId) {
  return (dispatch) => {
    if (!listenerId) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "You haven't entered an ID."
      });
      return false;
    }

    axios
      .get(`${globalStr.serverUrl}/listener/get/single/${listenerId}`)
      .then((res) => {
        const { noListenerDoc, listenerDoc } = res.data;
        if (noListenerDoc) {
          dispatch({
            type: CONSTS.SET_ERROR,
            payload: "Listener with this id doesn't exist."
          });
          return fa1se;
        }
        dispatch({ type: CONST.SET_LISTENER_DATA, payload: listenerDoc });
        dispatch({ type: CONSTS.SET_MESSAGE, payload: "Listener found!" });
      })
      .catch((e) => {
        reject(e);
        helpers.showErrorMessage(e);
      });
  };
}

export function registerListener(userName, number, bio, isTest) {
  return async (dispatch) => {
    if (!userName) {
      ToastAndroid.showWithGravity(
        "Name not entered",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return false;
    }
    if (!number) {
      ToastAndroid.showWithGravity(
        "Number not entered",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return false;
    }

    const pattern = / ^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$ /;
    if (pattern.test(number)) {
      ToastAndroid.showWithGravity(
        "Number must be Indian",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
    }

    if (!bio) {
      ToastAndroid.showWithGravity(
        "Bio not entered",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return false;
    }

    try {
      const registerRes = await axios.post(
        `${globalStr.serverUrl}/listener/register`,
        { number, bio, userName, isTest }
      );

      if (registerRes.status == 200) {
        dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: true });

        return true;
      }
    } catch (e) {
      if (e.response.status == 401) {
        console.log(e.response);
        if (e.response.data.isSame == "userName") {
          ToastAndroid.showWithGravity(
            "Username exists!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: false });
        } else if (e.response.data.isSame == "number") {
          ToastAndroid.showWithGravity(
            "Number exists!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: false });
          return false;
        } else if (e.response.data.numberNotIndian) {
          ToastAndroid.showWithGravity(
            "Number is not Indian!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: false });
          return false;
        }
      }
    }
  };
}

/*
export function verifyEmail(activationCode) {
  return (dispatch) => {
    if (!activationCode) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "You haven't entered an activation code.",
      });
      return false;
    }
    axios
      .put(`${globalStr.serverUrl}/listener/verify/email`, activationCode, {
        headers: { "x-auth-listener": helpers.getListenerToken() },
      })
      .then((res) => {
        const { emailVerified } = res.data;
        dispatch({
          type: CONSTANTS.SET_EMAIl_VERIFIED,
          payload: emailVerified,
        });
      })
      .catch((e) => {
        dispatch({ type: CONSTANTS.SET_ERROR, payload: e });
      });
  };
}
*/
export function requestOtp(number, isTest) {
  if (!number) {
    ToastAndroid.showWithGravity(
      "Number was not entered",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER
    );
  }

  return async (dispatch) => {
    try {
      const otpRes = await axios.put(
        `${globalStr.serverUrl}/listener/request/otp`,
        { number, isTest }
      );

      const otpSent = otpRes.data;

      if (!otpSent) {
        ToastAndroid.showWithGravity(
          "OTP send error",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
      }

      dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: true });
    } catch (e) {
      ToastAndroid.showWithGravity(e, ToastAndroid.SHORT, ToastAndroid.CENTER);
    }
  };
}

export function authListener(number, otp) {
  return async (dispatch) => {
    try {
      if (!number) {
        ToastAndroid.showWithGravity(
          "Number hasn't been entered",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );

        return false;
      }

      if (!otp) {
        ToastAndroid.showWithGravity(
          "OTP hasn't been entered",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );

        return false;
      }

      const authRes = await axios.post(`${globalStr.serverUrl}/listener/auth`, {
        number,
        password: otp
      });

      console.log(authRes);

      if (authRes.status == 200) {
        dispatch({ type: CONSTANTS.SET_TOKEN, payload: authRes.data.token });
        dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: false });
        dispatch({ type: CONSTANTS.SET_AVAILABLE, payload: true });
        return true;
      }
    } catch (e) {
      console.log(e);
      if (e.response.status == 401) {
        if (e.response.data.notSent === "loginString") {
          ToastAndroid.showWithGravity(
            "Number not sent!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          return false;
        } else if (e.response.data.notSent === "password") {
          ToastAndroid.showWithGravity(
            "OTP not sent!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          return false;
        }
      } else if (e.response.status == 404) {
        if (!e.response.data.isUser) {
          ToastAndroid.showWithGravity(
            "No such user!",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          return false;
        }
      } else if (e.response.status == 500) {
        ToastAndroid.showWithGravity(
          "OTP expired!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        dispatch({ type: CONSTANTS.SET_OTP_SENT, payload: false });
        return false;
      }
    }
  };
}

export function logoutListener() {
  return async (dispatch, getState) => {
    if (getState().listener.token) {
      dispatch({ type: CONSTANTS.SET_TOKEN, payload: null });
      return true;
    } else {
      ToastAndroid.showWithGravity(
        "Not logged in to log out",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      );
      return false;
    }
  };
}

export function toggleActiveStatus() {
  return async (dispatch, getState) => {
    const status = getState().listener.isAvailable;
    try {
      const toggleRes = await axios.put(
        `${globalStr.serverUrl}/listener/toggle/available`,
        {},
        { headers: { "x-auth-token-listener": getState().listener.token } }
      );

      if (toggleRes) {
        dispatch({ type: CONSTANTS.SET_AVAILABLE, payload: !status });
        ToastAndroid.showWithGravity(
          `You went ${!status ? "online" : "offline"}`,
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        return true;
      }
    } catch (e) {
      console.log(e.response);
    }
  };
}

/*
export function setAvatar(imgFile) {
  let avatarData = new FormData();
  avatarData.append("avatar", imgFile);

  return (dispatch) => {
    if (!imgFile) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "You haven't submitted an image.",
      });
      return false;
    }

    axios
      .put(`${globalStr.serverUrl}/listener/set/avatar`, avatarData, {
        headers: {
          "x-auth-listener": helpers.getListenerToken(),
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        const { avatarPath } = res.data;
        resolve(avatarPath);
        dispatch({ type: CONSTANTS.SET_AVATAR, payload: avatarPath });
      })
      .catch((e) => {
        dispatch({ type: CONSTANTS.SET_ERROR, payload: e });
      });
  };
}

export function changeEmail(newEmail) {
  return (dispatch) => {
    if (!newEmail) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "You haven't submitted an email.",
      });
      return false;
    }

    axios
      .put(
        `${globalStr.serverUrl}/listener/change/email`,
        { newEmail: newEmail },
        {
          headers: { "x-auth-listener": helpers.getListenerToken() },
        }
      )
      .then((res) => {
        const { verificationEmailSent } = res.data;
        if (verificationEmailSent) {
          dispatch({
            type: CONSTANTS.SET_MESSAGE,
            payload: "Verification email sent.",
          });
        }
        dispatch({ type: CONSTANTS.SET_AVATAR, payload: avatarPath });
      })
      .catch((e) => {
        dispatch({ type: CONSTANTS.SET_ERROR, payload: e });
      });
  };
}

export function setCategories(newCategories) {
  return (dispatch) => {
    if (!newCategories) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "You haven't submitted any categories.",
      });
      return false;
    }

    axios
      .put(`${globalStr.serverUrl}/listener/set/categories`, newCategories, {
        headers: { "x-auth-listener": helpers.getListenerToken() },
      })
      .then((res) => {
        const { categoriesSet } = res.data;
        if (categoriesSet) {
          dispatch({ type: CONSTANTS.SET_MESSAGE, pay: "Categories set." });
          dispatch({
            type: CONSTANTS.SET_CATEGORIES,
            payload: newCategories,
          });
        }
      })
      .catch((e) => {
        dispatch({ type: SET_ERROR, payload: e });
      });
  };
}

export function setOpPk(sessionId) {
  return (dispatch) => {
    if (!sessionId) {
      dispatch({
        type: CONSTANTS.SET_ERROR,
        payload: "Session ID not entered.",
      });
    }
    axios
      .get(`${serverUrl}/session/get/single/${sessionId}`)
      .then((res) => {
        const { seekerPk } = res.data;
        dispatch({ type: CONSTANTS.SET_OP_PK, payload: seekerPk });
      })
      .catch((e) => {
        dispatch({ type: CONSTANTS.SET_ERROR, payload: e });
      });
  };
}

//run this function on each login
export function setOwnPk() {
  return (dispatch) => {
    RSAKeychain.generate(ENV.KEYTAG)
      .then((pk) => dispatch({ type: CONSTANTS.SET_OWN_PK, payload: pk }))
      .catch((e) => dispatch({ type: CONSTANTS.SET_ERROR, payload: e }));
  };
}
*/
export default function ListenerStateReducer(
  state = initialState,
  action = {}
) {
  switch (action.type) {
    case CONSTANTS.SET_LISTENER_DATA:
      return {
        ...state,
        userName: action.payload.userName,
        approved: action.payload.approvalStatus.approved,
        dateRegistered: action.payload.creationDate,
        email: action.payload.email,
        avatar: action.payload.avatar.src,
        cellNumber: action.payload.cell,
        categories: action.payload.categories,
        banned:
          action.payload.bannedStatus.expireDate <
          new Date().toISOString.substr(0, 10)
            ? false
            : true,
        "status.engagedSession": action.payload.status.currentEngagedSessionId,
        "status.online": action.payload.status.online
      };
    case CONSTANTS.SET_STATUS:
      return {
        ...state,
        "status.online": action.payload
      };
    case CONSTANTS.SET_AVATAR:
      return {
        ...state,
        avatar: action.payload
      };
    case CONSTANTS.SET_CATEGORIES:
      return {
        ...state,
        categories: [...action.payload]
      };
    case CONSTANTS.SET_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case CONSTANTS.SET_MESSAGE:
      return {
        ...state,
        message: action.payload
      };
    case CONSTANTS.SET_EMAIl_VERIFIED:
      return {
        ...state,
        emailVerified: action.payloads
      };
    case CONSTANTS.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case CONSTANTS.SET_OP_PK:
      return {
        ...state,
        opPk: action.payload
      };
    case CONSTANTS.SET_OWN_PK:
      return {
        ...state,
        ownPk: action.payload
      };
    case CONSTANTS.SET_OTP_SENT:
      return {
        ...state,
        otpSent: action.payload
      };
    case CONSTANTS.SET_AVAILABLE:
      return {
        ...state,
        isAvailable: action.payload
      };
    default:
      return state;
  }
}
