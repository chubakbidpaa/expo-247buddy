/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

export const SET_LISTENER_DATA = "SET_LISTENER_DATA";
export const SET_STATUS = "SET_STATUS";
export const SET_AVATAR = "SET_AVATAR";
export const SET_CATEGORIES = "SET_CATEGORIES";
export const SET_ERROR = "SET_ERROR";
export const SET_OP_PK = "SET_OP_PK";
export const SET_MESSAGE = "SET_MESSAGE";
export const SET_EMAIl_VERIFIED = "SET_EMAIL_VERIFIED";
export const SET_TOKEN = "SET_TOKEN";
export const SET_OWN_PK = "SET_OWN_PK";
export const SET_OTP_SENT = "SET_OTP_SENT";
export const SET_AVAILABLE = "SET_AVAILABLE";