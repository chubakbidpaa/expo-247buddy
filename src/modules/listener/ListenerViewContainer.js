import { compose } from "recompose";
import { connect } from "react-redux";

import ListenerView from "./ListenerView";
import * as ListenerState from "./ListenerState";

export default compose(
  connect(
    (state) => ({
      cellNumber: state.listener.cellNumber,
      otpSent: state.listener.otpSent,
      token: state.listener.token,
      isAvailable: state.listener.isAvailable,
    }),
    (dispatch) => ({
      registerListener: (userName, number, bio, isTest) =>
        dispatch(ListenerState.registerListener(userName, number, bio, isTest)),
      requestOtp: (number, isTest) =>
        dispatch(ListenerState.requestOtp(number, isTest)),
      authListener: (number, otp) =>
        dispatch(ListenerState.authListener(number, otp)),
      logOut: () => dispatch(ListenerState.logoutListener()),
      toggleAvailable: () => dispatch(ListenerState.toggleActiveStatus()),
    })
  )
)(ListenerView);
