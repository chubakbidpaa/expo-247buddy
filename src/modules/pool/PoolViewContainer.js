import { compose } from "recompose";
import { connect } from "react-redux";
import * as PoolState from "./PoolState";
import PoolView from "./PoolView";


export default compose(
  connect(
    (state) => ({
      pool: this.state.pool,
      updateAuto: this.state.updateAuto
    }),
    (dispatch) => ({
      getPool: () =>
        dispatch(PoolState.getPool()),
      updatePool: () =>
        dispatch(PoolState.updatePool()),
      toggleUpdateAuto: () =>
        dispatch(PoolState.toggleUpdateAuto())

    })
  )
)(PoolView);
