import React, { Component } from "react";
import WaitingPoolPage from "../../components/stateful/WaitingPoolPage";

export default class PoolView extends Component {
  render() {
    return <WaitingPoolPage {...this.props} />;
  }
}