import axios from "axios";
import * as CONSTANTS from "./PoolConstants";
import { serverUrl } from "../../../global";
import * as Ably from "ably";
import * as ENV from "../../../env";
import { EventRegister } from "react-native-event-listeners";
import { ToastAndroid } from "react-native";

const initialState = {
  pool: [],
  updateAuto: true
};

export function getPool() {
  return async (dispatch, getState) => {
    try {
      const poolRes = await axios.get(`${serverUrl}/pool/get/`, { headers: { "x-auth-token-listener": getState().listener.token } });

      if (poolRes.status === 200) {
        dispatch({ type: CONSTANTS.SET_POOL, payload: poolRes.data.pool });
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };
}


export function updatePool() {
  return async (dispatch, getState) => {
    try {

      if (!getState().pool.updateAuto) {
        return false;
      }

      let newSessionId;
      let toRemove;
      const realtime = new Ably.Realtime(ENV.ABLY_API);
      const channel = realtime.channels.get("waiting-pool");

      EventRegister.addEventListener("received", data => {
        newSessionId = data;
      });

      EventRegister.addEventListener("expireAccept", data => {
        toRemove = data;
      });

      if (toRemove) {
        dispatch({ type: CONSTANTS.REMOVE_FROM_POOL, payload: toRemove });
      }

      if (newSessionId) {
        const poolRes = await axios.get(`${serverUrl}/pool/single/${newSessionId}`, { headers: { "x-auth-token-listener": getState().listener.token } });
        if (poolRes.status === 200) {

          if (!getState().chat.isChatting) {
            ToastAndroid.showWithGravity("New Seeker", ToastAndroid.SHORT, ToastAndroid.CENTER);
          }

          dispatch({ type: CONSTANTS.UPDATE_POOL, payload: poolRes.data.poolSingle });
        }

      }

      channel.subscribe("entered", (message) => {
        const data = JSON.parse(message.data);
        EventRegister.emit("received", data.sessionId);

      });

      channel.subscribe(["expired", "accepted"], message => {
        const data = JSON.parse(message.data);
        EventRegister.emit("expireAccept", data);
      });

    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };
}

export function toggleUpdateAuto(toggle) {
  return {
    type: CONSTANTS.SET_UPDATE_AUTO,
    payload: toggle
  };
}

export default function PoolStateReducer(
  state = initialState,
  action = {}
) {
  switch (action.type) {
    case CONSTANTS.SET_POOL:
      return {
        ...state,
        pool: action.payload
      };
    case CONSTANTS.UPDATE_POOL:
      return {
        ...state,
        pool: [...state.pool, action.payload]
      };
    case CONSTANTS.SET_UPDATE_AUTO:
      return {
        ...state,
        updateAuto: action.payload
      };
    case CONSTANTS.REMOVE_FROM_POOL:
      return {
        ...state,
        pool: state.pool.filter(item => {
          return item.sessionId === action.payload;
        })
      };
  }
}