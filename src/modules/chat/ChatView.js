/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

import React, { Component } from "react";

import ChatPage from "../../components/stateful/Chat/ChatPage";
import SeekerLounge from "../../components/stateful/SeekerLoungePage";
import StartSession from "../../components/stateful/StartSession";


export default class ChatView extends Component {
  render() {
    if (this.props.switcher === "start") {
      return <StartSession startSession={this.props.startSession} navigation={this.props.navigation}/>;
    } else if (this.props.switcher === "chat") {
      return <ChatPage {...this.props} />;
    } else if (this.props.switcher === "lounge") {
      return <SeekerLounge seekerLounge={this.props.seekerLounge} cancelSession={this.props.cancelSession}
                           navigation={this.props.navigation}/>;
    }
  }
}
