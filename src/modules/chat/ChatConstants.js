/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

export const SET_ERROR = "SET_ERROR";
export const SET_ACCEPTED_BY_LISTENER = "SET_ACCEPTED_BY_LISTENER";
export const SET_MESSAGES = "SET_MESSAGES";
export const SET_NAME = "SET_NAME";
export const SET_IS_CHATTING = "SET_IS_CHATTING";
export const SET_SESSION_ID = "SET_SESSION_ID";
export const SET_PARTNER_LEFT = "SET_PARTNER_LEFT";