/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

import * as CONSTANTS from "./ChatConstants";
import axios from "axios";
import { serverUrl } from "../../../global";
import { ToastAndroid } from "react-native";
import * as Ably from "ably";
import * as ENV from "../../../env";
import { EventRegister } from "react-native-event-listeners";
import moment from "moment";
import { get } from "react-native/Libraries/TurboModule/TurboModuleRegistry";

const initialState = {
  user: null,
  sessionId: null,
  isChatting: false,
  partnerLeft: false,
  acceptedByListener: false,
  messages: []

};

export function startSession(seekerNumber, seekerReason) {
  return async dispatch => {
    if (!seekerNumber) {
      ToastAndroid.showWithGravity("Number not entered", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return false;
    }

    if (!seekerReason) {
      ToastAndroid.showWithGravity("Reason not entered", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return false;
    }

    try {
      const sessionRes = axios.post(`${serverUrl}/session/createSession`, { seekerNumber, seekerReason });

      if (sessionRes.status === 200) {
        await axios.post(`${serverUrl}/poolop/entered/${sessionRes.data.sessionId}`);
        dispatch({ type: CONSTANTS.SET_SESSION_ID, payload: sessionRes.data.sessionId });
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };
}

export function acceptSession(sessionId) {
  return async (dispatch, getState) => {
    if (!sessionId) {
      ToastAndroid.showWithGravity("No Session ID!", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return false;
    }

    try {
      const acceptRes = await axios.put(`${serverUrl}/session/accept/${sessionId}`,
        {}, { headers: { "x-auth-token-listener": getState().listener.token } });
      await axios.post(`${serverUrl}/chat/accept/${getState().chat.sessionId}`, {}, {
        headers: {
          "x-session-id": sessionId,
          "x-auth-token-listener": getState().listener.token
        }
      });
      await axios.post(`${serverUrl}/poolop/accepted/${sessionId}`);
      if (acceptRes.status === 200) {
        dispatch({ type: CONSTANTS.SET_SESSION_ID, payload: sessionId });
        dispatch({ type: CONSTANTS.SET_NAME, payload: "Listener" });
        dispatch({ type: CONSTANTS.SET_IS_CHATTING, payload: true });
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }

  };
}

export function cancelSession() {
  return async (dispatch, getState) => {
    try {
      await axios.post(`${serverUrl}/poolop/expired/${getState().chat.sessionId}`);

      dispatch({ type: CONSTANTS.SET_SESSION_ID, payload: null });

      return true;
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }

  };
}


export function seekerLounge() {
  return async (dispatch, getState) => {
    const realtime = new Ably.Realtime(ENV.ABLY_API);
    const channel = realtime.channels.get(getState().chat.sessionId);
    let isExpired;
    let isAccepted;

    EventRegister.addEventListener("expire", async (data) => {
      isExpired = data;
      try {
        await axios.put(`${serverUrl}/session/expire/${getState().chat.sessionId}`);
      } catch (e) {
        console.log(e);
        throw new Error(e);
      }
    });

    EventRegister.addEventListener("accepted", (data) => {
      isAccepted = data;
    });

    if (isAccepted) {
      dispatch({ type: CONSTANTS.SET_ACCEPTED_BY_LISTENER, payload: true });
      dispatch({ type: CONSTANTS.SET_NAME, payload: "Seeker" });
      dispatch({ type: CONSTANTS.SET_IS_CHATTING, payload: true });
      ToastAndroid.showWithGravity("Session Accepted", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return true;
    }

    if (isExpired) {
      ToastAndroid.showWithGravity("Session expired", ToastAndroid.SHORT, ToastAndroid.CENTER);
      await axios.post(`${serverUrl}/poolop/expired/${getState().chat.sessionId}`);
      return false;
    }


    const checkTime = (startTime) => {
      const now = moment();

      if (now.diff(startTime, "minutes") > 3) {
        EventRegister.emit("expire", true);
      }
    };

    setTimeout(checkTime, 1000);

    channel.subscribe("accepted", (message) => {
      if (JSON.parse(message.data).sessionId !== getState().chat.sessionId) throw new Error("SessionIDs don't match.");
      EventRegister.emit("accepted", true);
    });

  };
}


export function endSession() {
  return async (dispatch, getState) => {
    try {
      const endRes = await axios(`${serverUrl}/session/disconnect/${getState().chat.sessionId}`);
      await axios.post(`${serverUrl}/chat/leave`, {}, { headers: { "x-session-id": getState().chat.sessionId } });

      if (endRes.status === 200) {
        dispatch({ type: CONSTANTS.SET_SESSION_ID, payload: null });
        dispatch({ type: CONSTANTS.SET_ACCEPTED_BY_LISTENER, payload: false });
        dispatch({ type: CONSTANTS.SET_MESSAGES, payload: null });
        dispatch({ type: CONSTANTS.SET_NAME, payload: null });
        dispatch({ type: CONSTANTS.SET_IS_CHATTING, payload: false });
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error((e));
    }
  };
}


export function sendMessage(message) {
  return async (dispatch, getState) => {
    try {
      const sendRes = axios.post(`${serverUrl}/chat/sendMessage`, {
        msg: message,
        user: getState().chat.user
      }, { headers: { "x-session-id": getState().chat.sessionId } });

    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };
};

export function messageWatch() {
  return async (dispatch, getState) => {
    try {
      const realtime = new Ably.Realtime(ENV.ABLY_API);
      const channel = realtime.channels.get(getState().chat.sessionId);

      channel.subscribe("message", message => {
        const data = JSON.parse(message.data);

        dispatch({ type: CONSTANTS.SET_MESSAGES, payload: data });

      });

      channel.subscribe("left", message => {
        dispatch({ type: CONSTANTS.SET_PARTNER_LEFT, payload: true });
      });

    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };

}


export default function ChatStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case CONSTANTS.SET_SESSION_ID:
      return {
        ...state,
        sessionId: action.payload
      };
    case CONSTANTS.SET_IS_CHATTING:
      return {
        ...state,
        isChatting: action.payload
      };
    case CONSTANTS.SET_MESSAGES:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      };
    case CONSTANTS.SET_NAME:
      return {
        ...state,
        user: action.payload
      };
    case CONSTANTS.SET_ACCEPTED_BY_LISTENER:
      return {
        ...state,
        acceptedByListener: action.payload
      };
    case CONSTANTS.SET_PARTNER_LEFT:
      return {
        ...state,
        partnerLeft: action.payload
      };
    default:
      return state;
  }
}
