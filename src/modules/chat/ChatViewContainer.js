import { compose } from "recompose";
import { connect } from "react-redux";

import ChatView from "./ChatView";
import * as ChatState from "./ChatState";

export default compose(
  connect(
    (state) => ({
      user: state.chat.user,
      sessionId: state.chat.sessionId,
      isChatting: state.chat.isChatting,
      partnerLeft: state.chat.partnerLeft,
      acceptedByListener: state.chat.acc,
      messages: []
    }),
    (dispatch) => ({
      startSession: (number, reason) => dispatch(ChatState.startSession(number, reason)),
      acceptSession: sessionId => dispatch(ChatState.acceptSession(sessionId)),
      endSession: () => dispatch(ChatState.endSession()),
      seekerLounge: () => dispatch(ChatState.seekerLounge()),
      messageWatch: () => dispatch(ChatState.messageWatch()),
      sendMessage: (message) => dispatch(ChatState.sendMessage(message)),
      cancelSession: () => dispatch(ChatState.cancelSession()),


    })
  )
)(ChatView);
